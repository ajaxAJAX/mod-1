.. proyect1 documentation master file, created by
   sphinx-quickstart on Sat Jan  9 14:50:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentacion de proyecto final
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Analisis y modelado:
   
   datos/introduccion
   datos/posicionamiento
   datos/objetivos
   datos/casos
   datos/reglas
   datos/requerimientos
   datos/conceptual
   datos/inter



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
