posicionamiento
======================

Especificación del problema 
--------------------------------
Se tiene como objetivos principales llevar el control del inventario en el almacén, tener la información actualizada referente a productos y proveedores, contar con información de costos por salidas y entradas de productos en el almacén.
Se espera tener una operación más ágil, que pueda aumentar la productividad de la operación del almacén, la cual se verá reflejada en la disminución de las compras de emergencia que son realizadas casi a diario, también controlar la fluctuación de precios de adquisición, y en la reducción de gastos de operación.

Stakeholders
--------------------------------

+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Nombre                | Roles             | Contacto              | Influencia    |
+=======================+===================+=======================+===============+
|                       |                   |                       |               |
| Pepe Grillo           | Almacenista       | Ext 4532              | Alta          |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Genaro Luna           | Proveedor         | Tel 55-22-33-45-11    | Baja          |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Mauricio Hernández    | Administración    | Ext 3322              | Media         |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Pedro Infante         | Jefe de cocina    | Ext 8976              | Alta          |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Ana Torres            | Analista          | Tel 55-23-11-67-89    | Alta          |
+-----------------------+-------------------+-----------------------+---------------+

Historias de usuario
--------------------------------

+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Como...                 | Quiero ...                                              | Para ...                                       | Complejidad    |
+=========================+=========================================================+================================================+================+
|                         |                                                         |                                                |                |
| Almacenista             | Capturar las facturas.                                  | Poder tener las existencias al día             | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Almacenista             | Capturar los pedidos.                                   | Poder descontar del inventario las salidas.    | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Almacenista             | Imprimir reportes de movimientos                        | Justificar los movimientos en el almacén.      | Alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Jefe de departamento    | Realizar pedidos al almacén                             | Satisfacer las necesidades de la operación.    | Baja           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Jefe de departamento    | Obtener reportes de mis gastos diarios.                 | Bajar los costos de operación.                 | Alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Jefe de departamento    | Que los precios de los productos estén actualizados.    | Para realizar mis pedidos.                     | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Admin                   | Actualizar información de productos.                    | Que se puedan capturar las facturas al día.    | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Admin                   | Poder eliminar productos que no se usan.                | Mantener más organizado el inventario.         | Alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Admin                   | Hacer ajustes en los movimientos                        | Corregir errores de captura.                   | Alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Proveedor               | Entregar mis facturas sin errores                       | Que se pueda programar mi pago.                | Baja           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| proveedor               | Se me reciban mis productos                             | Para que no se modifique mi factura.           | Baja           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+