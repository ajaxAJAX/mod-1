
Diagramas de estado
=========================

Capturar pedido
-----------------------
.. image:: images/DE-01.png


Capturar factura
------------------------
.. image:: images/DE-02.png


Diagramas de secuencias
===========================

capturar factura
---------------------------------
.. image:: images/sec_factura.png

Caso alterno capturar factura
-----------------------------------
.. image:: images/sec_factura-no-existe-producto.png

Diagrama de Actividades
==========================

Diagrama de Actividad
-------------------------

.. image:: images/AD-01.png


