Introducción
===============
El almacén tiene como propósito fundamental proveer al restaurante de las materias primas suficiente para poder operar, ya que su papel es vital y de suma importancia para ofrecer un servicio de calidad y además teniendo en cuenta que el almacén cuenta con uno de los activos más grandes, ya que su inventario se refleja en el balance general siendo el activo corriente más grande e importante.

Actualmente en el almacén del restaurante,  no se cuenta con un sistema informático, todos los movimientos como entradas y salidas de productos son ingresados en hojas de cálculo, no existe un registro de proveedores, tampoco se cuenta con inventario actualizado. El incremento de las operaciones diarias por la gran afluencia de clientes ha elevado las compras y salidas del almacén  que han desencadenado una serie de fallas en la operación del restaurante que afecta a todas las áreas, como son el departamento de compras, costos, y en el servicio en general.   

Propósito del sistema
~~~~~~~~~~~~~~~~~~~~~~
Se busca tener un control exacto de las existencias en el almacén para poder reducir los gastos en las compras de insumos para la operación del restaurante, conocer los gastos diarios de la cocina para poder utilizar esa información en la generación de mínimos y máximos con los que pueda trabajar el restaurante, teniendo como finalidad bajar los gastos en un 30  %.

Alcance del sistema
~~~~~~~~~~~~~~~~~~~~~~~
Desarrollar y diseñar un sistema de inventarios que cubra las necesidades de la operación del almacén, como son captura de facturas, registro de proveedores, listados de productos, captura de requisiciones, totales de compras, totales de salida, inventario actualizado. El sistema será desarrollado en un entorno web, ya que se tiene contemplado acceder a los productos y precios disponibles de los proveedores por esta vía.





